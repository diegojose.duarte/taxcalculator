package br.com.diego.tax.calculator;

public class Calculator {

    /**
     *
     */
    private static final double ALIQUOTA_1 = 0.075;

    /**
     *
     */
    private static final double ALIQUOTA_2 = 0.15;

    /**
     *
     */
    private static final double ALIQUOTA_3 = 0.225;

    /**
     *
     */
    private static final double ALIQUOTA_4 = 0.275;


    /**
     *
     * @return
     */
    public static double getAliquota1() {
        return ALIQUOTA_1;
    }

    /**
     *
     * @return
     */
    public static double getAliquota2() {

        return ALIQUOTA_2;
    }

    /**
     *
     * @return
     */
    public static double getAliquota3() {
        return ALIQUOTA_3;
    }

    /**
     *
     * @return
     */
    public static double getAliquota4() {
        return ALIQUOTA_4;
    }

    /**
     *
     * @param salary
     * @return
     */
    public double getTaxFromSalary(double salary) {

        if (salary > 1903.98 && salary <= 2826.65) {
            return salary * Calculator.getAliquota1();
        }

        if (salary > 2826.65 && salary <= 3751.05) {
            return salary * Calculator.getAliquota2();
        }

        if (salary > 3751.05 && salary <= 4664.68) {
            return salary * Calculator.getAliquota3();
        }

        if (salary > 4664.68) {
            return salary * Calculator.getAliquota4();
        }

        return 0;
    }

    /**
     *
     * @param salary
     * @return
     */
    public double getLquidSalary(double salary){
        return salary - this.getTaxFromSalary(salary);
    }



}
