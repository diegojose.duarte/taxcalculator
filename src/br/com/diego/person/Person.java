package br.com.diego.person;

public class Person {
    private String cpf;
    private float salary;
    private String name;

    public String getCpf() {
        return cpf;
    }

    public String getFormattedCpf() {

        String cpf = new String(this.cpf);

        String bloco1 = cpf.substring(0, 3);
        String bloco2 = cpf.substring(3, 6);
        String bloco3 = cpf.substring(6, 9);
        String bloco4 = cpf.substring(9, 11);
        cpf = bloco1+"."+bloco2+"."+bloco3+"-"+bloco4;

        return cpf;
    }

    public void setCpf(String cpf) {

        cpf = cpf.replaceAll("(\\W)","");

        this.cpf = cpf;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Nome: " + this.getName() + "\nCPF: " + this.getFormattedCpf() + "\nSalário: "+ this.getSalary();
    }
}
