package br.com.diego;

import br.com.diego.person.Person;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        InputStream is = null;
        BufferedReader br = null;

        is = System.in;
        br = new BufferedReader(new InputStreamReader(is));

        ArrayList<Person> list = new ArrayList<>();

        for (int i = 0; i < 1; i++) {
            String name = null;
            String cpf = null;
            String salary = null;


            try {
                System.out.println("Digite o nome:");
                name = br.readLine();

                System.out.println("Digite o CPF:");
                cpf = br.readLine();

                System.out.println("Digite o Salário:");
                salary = br.readLine();

                Person p = new Person();

                p.setName(name);
                p.setCpf(cpf);
                p.setSalary(Float.parseFloat(salary));

                list.add(p);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }
}